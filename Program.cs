﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace ChatServer
{
    class Program
    {
        private static List<TcpClient> _clients = new List<TcpClient>();

        static void Main(string[] args)
        {
            var listener = new TcpListener(IPAddress.Any, 8888);
            listener.Start();
            while (true)
            {
                var client = listener.AcceptTcpClient();
                _clients.Add(client);
                
                new Thread(() =>
                {
                    try
                    {
                        var reader = new StreamReader(client.GetStream());
                        var writer = new StreamWriter(client.GetStream());

                        writer.WriteLine("Bienvenido al chat");
                        writer.Flush();

                        while (true)
                        {
                            var line = reader.ReadLine();
                            
                            foreach (TcpClient c in _clients)
                            {
                                var ConnectedClientsWriter = new StreamWriter(c.GetStream());
                                ConnectedClientsWriter.WriteLine(line);
                                ConnectedClientsWriter.Flush();
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        _clients.Remove(client);
                        Console.WriteLine($"Error: {e.Message}");
                    }
                }).Start();
            }
        }
    }
}
